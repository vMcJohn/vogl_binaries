/**************************************************************************
 *
 * Copyright 2013-2014 RAD Game Tools and Valve Software
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <argp.h>
#include <libgen.h>

#include <algorithm>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

struct tracer_cmdline_opts_t
{
    const char *name;
    unsigned int num_values;
    const char *desc;
    int group;
    const char *arg_name;
};

static const tracer_cmdline_opts_t g_tracer_cmdline_opts[] =
{
    { "vogl_tracefile", 1, "Tracefile name.", 10, "TRACEFILE" },
    { "vogl_logfile", 1, "Spew output to logfile.", 10, "LOGFILE" },

    { "vogl_pause", 0, "Pause until debugger attaches.", 20, NULL },
    { "vogl_quiet", 0, "Don't print vogl warnings.", 20, NULL },
    { "vogl_verbose", 0, "Print verbose messages.", 20, NULL },
    { "vogl_debug", 0, "Print debug messages and run extra validation code.", 20, NULL },

    { "vogl_force_debug_context", 0, "Use OpenGL debug context.", 30, NULL },

    { "vogl_backtrace_all_calls", 0, "Get backtraces for all gl calls.", 30, NULL },
    { "vogl_backtrace_no_calls", 0, "Don't backtrace any calls.", 30, NULL },

    { "vogl_dump_gl_calls", 0, "Dump all gl calls.", 40, NULL },
    { "vogl_dump_gl_buffers", 0, "Dump all gl buffers.", 40, NULL },
    { "vogl_dump_gl_shaders", 0, "Dump all gl shaders.", 40, NULL },
    { "vogl_dump_gl_full", 0, "Dump all gl calls, buffers, and shaders.", 40, NULL },
    { "vogl_sleep_at_startup", 1, "Sleep for X seconds at startup.", 40, "TIME" },
    { "vogl_flush_files_after_each_call", 0, "Flush trace files after each packet write.", 40, NULL },
    { "vogl_flush_files_after_each_swap", 0, "Flush trace files after glXSwapBuffer.", 40, NULL },
    { "vogl_disable_signal_interception", 0, "Don't set exception handler.", 40, NULL },
    { "vogl_tracepath", 1, "Default tracefile path.", 40, NULL },
    { "vogl_dump_png_screenshots", 0, "Save png screenshots.", 40, NULL },
    { "vogl_dump_jpeg_screenshots", 0, "Save jpeg screenshots.", 40, NULL },
    { "vogl_jpeg_quality", 0, "Screenshot jpeg quality.", 40, NULL },
    { "vogl_screenshot_prefix", 1, "Screenshot prefix", 40, "PREFIX" },
    { "vogl_hash_backbuffer", 0, "Do crc64 for framebuffer.", 40, NULL },
    { "vogl_sum_hashing", 0, "Do sum64 for framebuffer.", 40, NULL },
    { "vogl_dump_backbuffer_hashes", 1, "Print backbuffer hash values.", 40, "FILENAME" },
    { "vogl_null_mode", 0, "Simple null renderer.", 40, NULL },
    { "vogl_disable_client_side_array_tracing", 0, "Disable client side arrays which can impact performance.", 40, NULL },
    { "vogl_disable_gl_program_binary", 0, "Remove GL_ARB_get_program_binary.", 40, NULL },
    { "vogl_exit_after_x_frames", 1, "Call exit() after X frames.", 40, "COUNT" },
#ifdef VOGL_REMOTING
    { "vogl_traceport", 1, "Vogl remote port.", 50 },
#endif
#if VOGL_FUNCTION_TRACING
    { "vogl_func_tracing", 0, "Print called vogl functions.", 50 },
#endif
};

/*
 * To kill dialog asking if you want to launch the game:
 * You also need a steam_dev.cfg in your steam install folder (next to steam.sh) with:
 * bPromptGameLaunchBypass 1
 *
 * in it so you don't get a dialog bugging you before letting the game run.
 */

static const struct
{
    int id;
    const char *name;
} g_gameids[] =
{
    { 214910,   "AirConflicts" },
    { 400,      "Portal1" },
    { 218060,   "BitTripRunner" },
    { 570,      "Dota2" },
    { 35720,    "Trine2" },
    { 440,      "TF2" },
    { 41070,    "Sam3" },
    { 1500,     "Darwinia" },
    { 550,      "L4D2" },
    { 1500,     "Darwinia2" },
    { 570,      "Dota2Beta" },
    { 221810,   "TheCave" },
    { 220200,   "KerbalSpaceProgram" },
    { 44200,    "GalconFusion" },
    { 201040,   "GalconLegends" },
    { 25000,    "Overgrowth" },
    { 20920,    "TheWitcher2" },
    { 211820,   "Starbound" } // 64-bit game
};

#define F_XTERM     0x00000001
#define F_DRYRUN    0x00000002
#define F_LDDEBUG   0x00000004

struct arguments_t
{
    unsigned int flags;
    std::string cmdline;
    std::string vogl_tracefile;
    std::string vogl_logfile;
    std::string gameid;
    std::string game_args;
};

static error_t
parse_opt(int key, char *arg, struct argp_state *state)
{
    arguments_t *arguments = (arguments_t *)state->input;

    switch (key)
    {
    case 0:
        if (arg && arg[0])
        {
            if (arguments->gameid.size())
            {
                // These are arguments to our program, e.g. if the user wants to run:
                //   "voglperfrun -- glxgears -info",
                // we get "-info".
                arguments->game_args += std::string("\"") + state->argv[state->next - 1] + "\" ";
            }
            else
            {
                arguments->gameid = arg;
            }
        }
        break;
    case '?':
        fprintf(stderr, "\nUsage:\n");
        fprintf(stderr, "  vogl_trace.sh glxspheres64\n");
        fprintf(stderr, "  vogl_trace.sh 440\n");
        fprintf(stderr, "  vogl_trace.sh --vogl_backtrace_no_calls TheWitcher2\n");

        fprintf(stderr, "\nGameIDS:\n");
        for(size_t i = 0; i < sizeof(g_gameids) / sizeof(g_gameids[0]); i++)
        {
            fprintf(stderr, "  %-6d - %s\n", g_gameids[i].id, g_gameids[i].name);
        }
        printf("\n");
        argp_state_help(state, stderr, ARGP_HELP_LONG | ARGP_HELP_EXIT_OK);
        break;

    case -1:
    {
        // Grab the argument name. Will be something like "--vogl_logfile=/tmp/blah.log".
        std::string argname = state->argv[state->next - 1];

        size_t equalpos = argname.find('=');
        if (equalpos != std::string::npos)
        {
            // Convert "option=val" to "option val".
            argname[equalpos] = ' ';

            // Check if this is vogl_logfile or vogl_tracefile as we always add those later.
            std::string opt = argname.substr(0, equalpos);
            if (opt == "--vogl_logfile")
            {
                arguments->vogl_logfile = argname.substr(equalpos + 1);
                break;
            }
            else if (opt == "--vogl_tracefile")
            {
                arguments->vogl_tracefile = argname.substr(equalpos + 1);
                break;
            }
        }

        // Add this libvogltrace option.
        arguments->cmdline += " ";
        arguments->cmdline += argname;
        break;
    }

    case 'x': arguments->flags |= F_XTERM; break;
    case 'y': arguments->flags |= F_DRYRUN; break;
    case 'd': arguments->flags |= F_LDDEBUG; break;
        break;

    case -2:
        // --show-type-list: Whitespaced list of options for bash autocomplete.
        for (size_t i = 0; ; i++)
        {
            const char *name = state->root_argp->options[i].name;
            if (!name)
                break;
            printf("--%s ", name);
        }
        exit(0);
    }
    return 0;
}

void
errorf(const char *format, ...)
{
    va_list args;

    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);

    exit(-1);
}

//----------------------------------------------------------------------------------------------------------------------
// Printf style formatting for std::string.
//----------------------------------------------------------------------------------------------------------------------
std::string
string_format(const char *fmt, ...)
{
    std::string str;
    int size = 256;

    for (;;)
    {
        va_list ap;

        va_start(ap, fmt);
        str.resize(size);
        int n = vsnprintf((char *)str.c_str(), size, fmt, ap);
        va_end(ap);

        if ((n > -1) && (n < size))
        {
            str.resize(n);
            return str;
        }

        size = (n > -1) ? (n + 1) : (size * 2);
    }

    return str;
}

std::string
url_encode(const std::string &value)
{
    std::ostringstream escaped;

    escaped.fill('0');
    escaped << std::hex;

    for (std::string::const_iterator i = value.begin(), n = value.end(); i != n; ++i)
    {
        std::string::value_type c = (*i);

        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~')
            escaped << c;
        else if (c == ' ')
            escaped << "%20";
        else
            escaped << '%' << std::setw(2) << ((int) c) << std::setw(0);
    }

    return escaped.str();
}

std::string
getfullpath(std::string filename)
{
    char buf[PATH_MAX];

    // Get the full path to our executable. It should be running in vogl/bin/x86_64 (or i386).
    if ((readlink("/proc/self/exe", buf, sizeof(buf)) <= 0))
        errorf("ERROR: readlink failed '%s.'\n", strerror(errno));

    // Get just the directory name and relative path to libvogltrace.so.
    std::string filedir = dirname(buf);

    filedir += "/../../vogl_build/" + filename;

    // Trim all the relative path parts.
    char *vogltracepath = realpath(filedir.c_str(), NULL);
    if (!vogltracepath)
    {
        printf("\nWARNING: realpath failed: '%s.' Does '%s' exist?\n", strerror(errno), filename.c_str());
        return filename;
    }

    // Make sure our libvogltrace.so exists.
    if(access(vogltracepath, F_OK) != 0)
    {
        printf("\nWARNING: %s file does not exist.\n", vogltracepath);
        return filename;
    }

    return vogltracepath;
}

int
main(int argc, char **argv)
{
    static const struct argp_option s_options[] =
    {
        { "dry-run",         'y', 0, 0, "Don't execute commands." },
        { "lddebug",         'd', 0, 0, "Debug spew from LD_DEBUG=lib."},
        { "xterm",           'x', 0, 0, "Launch with xterm." },
        { "show-type-list",  -2,  0, OPTION_HIDDEN, "Produce list of whitespace-separated words used for command completion." },
        { "help",            '?', 0, 0, "Give this help message." },
    };

    std::vector<argp_option> options;

    // Add the above options.
    options.insert(options.begin(), s_options, s_options + sizeof(s_options) / sizeof(s_options[0]));

    // Add libvogltrace options.
    for (size_t i = 0; i < sizeof(g_tracer_cmdline_opts) / sizeof(g_tracer_cmdline_opts[0]); i++)
    {
        struct argp_option opt;

        opt.name = g_tracer_cmdline_opts[i].name;
        opt.key = -1;
        opt.arg = g_tracer_cmdline_opts[i].arg_name;
        opt.flags = 0;
        opt.doc = g_tracer_cmdline_opts[i].desc;
        opt.group = g_tracer_cmdline_opts[i].group;
        options.push_back(opt);
    }

    // Add options terminator.
    struct argp_option opt_end = { 0 };
    options.push_back(opt_end);

    arguments_t args;
    args.flags = 0;
    struct argp argp = { &options[0], parse_opt, 0, "Steam Launcher helper app." };

    argp_parse(&argp, argc, argv, ARGP_NO_HELP, 0, &args);

    if (!args.gameid.size())
        errorf("ERROR: No gameid specified.\n");

    bool is_steam_file = true;
    if (atoi(args.gameid.c_str()) == 0)
    {
        if (access(args.gameid.c_str(), F_OK) == 0)
        {
            char *filename = realpath(args.gameid.c_str(), NULL);
            if (filename)
            {
                // This is a local executable.
                is_steam_file = false;
                args.gameid = filename;
                free(filename);
            }
        }
        else
        {
            // lower case what the user gave us.
            std::transform(args.gameid.begin(), args.gameid.end(), args.gameid.begin(), ::tolower);
            // Try to find the name and map it back to the id.
            for (size_t i = 0; i < sizeof(g_gameids) / sizeof(g_gameids[0]); i++)
            {
                std::string name = g_gameids[i].name;
                std::transform(name.begin(), name.end(), name.begin(), ::tolower);
                if (name == args.gameid)
                {
                    args.gameid = string_format("%d", g_gameids[i].id);
                    break;
                }
            }
        }
    }

    int gameid = is_steam_file ? atoi(args.gameid.c_str()) : -1;
    if (!gameid)
        errorf("ERROR: Could not find game number for %s\n", args.gameid.c_str());

    std::string gameid_str;
    if (is_steam_file)
    {
        gameid_str = "gameid" + args.gameid;
        printf("\nGameID: %d", gameid);

        for (size_t i = 0; i < sizeof(g_gameids) / sizeof(g_gameids[0]); i++)
        {
            if (gameid == g_gameids[i].id)
            {
                printf(" (%s)", g_gameids[i].name);
                gameid_str = g_gameids[i].name;
                break;
            }
        }

        printf("\n");
    }
    else
    {
        gameid_str = basename((char *)args.gameid.c_str());
        printf("\nGame: %s\n", args.gameid.c_str());
    }

    if (!args.vogl_tracefile.size() || !args.vogl_logfile.size())
    {
        char timestr[200];
        time_t t = time(NULL);

        timestr[0] = 0;
        struct tm *tmp = localtime(&t);
        if (tmp)
        {
            strftime(timestr, sizeof(timestr), "%Y_%m_%d-%H_%M_%S", tmp);
        }

        std::string fname = string_format("%s/vogltrace.%s.%s", P_tmpdir, gameid_str.c_str(), timestr);
        if (!args.vogl_tracefile.size())
            args.vogl_tracefile = fname + ".bin";
        if (!args.vogl_logfile.size())
            args.vogl_logfile = fname + ".log";
    }

    printf("\n");
    printf("Tracefile: %s\n", args.vogl_tracefile.c_str());
    printf("Logfile: %s\n", args.vogl_logfile.c_str());

    std::string vogltracepath32 = getfullpath("libvogltrace32.so");
    std::string vogltracepath64 = getfullpath("libvogltrace64.so");

    // set up LD_PRELOAD string
    std::string LD_PRELOAD = "LD_PRELOAD=\"";
    LD_PRELOAD += vogltracepath32 + ":" + vogltracepath64;

    if (is_steam_file || getenv("LD_PRELOAD"))
        LD_PRELOAD += ":$LD_PRELOAD";
    LD_PRELOAD += "\" ";
    printf("\n%s\n", LD_PRELOAD.c_str());

    // set up VOGL_CMD_LINE string
    std::string VOGL_CMD_LINE = "VOGL_CMD_LINE=\"";
    VOGL_CMD_LINE += "--vogl_tracefile " + args.vogl_tracefile;
    VOGL_CMD_LINE += " --vogl_logfile " + args.vogl_logfile;
    VOGL_CMD_LINE += args.cmdline;
    VOGL_CMD_LINE += "\"";
    printf("\n%s\n", VOGL_CMD_LINE.c_str());

    std::string LD_DEBUG = "";
    if (args.flags & F_LDDEBUG)
    {
        LD_DEBUG += "LD_DEBUG=libs ";
    }

    if (is_steam_file)
    {
        // set up xterm string
        std::string steam_cmd_str;
        if (args.flags & F_XTERM)
        {
            steam_cmd_str = "xterm -geom 120x80+20+20";
            const char *env_user = getenv("USER");

            // If this is mikesart, specify using the Consolas font (which he likes).
            if (env_user && !strcmp(env_user, "mikesart"))
                steam_cmd_str += " -fa Consolas -fs 10";

            // Add the xterm command part.
            steam_cmd_str += " -e ";
        }

        steam_cmd_str += "%command%";

        printf("\nSTEAMCMD: %s\n", steam_cmd_str.c_str());

        // set up steam string
        std::string steam_str = "steam steam://run/" + args.gameid + "//";
        std::string steam_args = VOGL_CMD_LINE + " " + LD_PRELOAD + LD_DEBUG + steam_cmd_str;
        std::string steam_fullcmd = steam_str + url_encode(steam_args);

        // Spew this whole mess out.
        printf("\nURL encoded launch string:\n%s\n", steam_fullcmd.c_str());

        // And launch it...
        if (!(args.flags & F_DRYRUN))
            system(steam_fullcmd.c_str());
    }
    else
    {
        std::string system_cmd = VOGL_CMD_LINE + " " + LD_PRELOAD + " " + args.gameid + " " + args.game_args;

        printf("\nlaunch string:\n%s\n", system_cmd.c_str());

        if (!(args.flags & F_DRYRUN))
            system(system_cmd.c_str());
    }

    return 0;
}
